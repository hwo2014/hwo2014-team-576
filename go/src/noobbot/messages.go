package main

import (
	"encoding/json"
	"log"
)

type MsgType string

const (
	Join           MsgType = "join"
	JoinRace       MsgType = "joinRace"
	YourCar        MsgType = "yourCar"
	GameInit       MsgType = "gameInit"
	GameStart      MsgType = "gameStart"
	CarPositions   MsgType = "carPositions"
	Throttle       MsgType = "throttle"
	TournamentEnd  MsgType = "tournamentEnd"
	Crash          MsgType = "crash"
	Spawn          MsgType = "spawn"
	SwitchLane     MsgType = "switchLane"
	TurboAvailable MsgType = "turboAvailable"
	Turbo          MsgType = "turbo"
	TurboStart     MsgType = "turboStart"
)

type BaseMessage struct {
	MsgType MsgType `json:"msgType"`
}

type ThrottleMessage struct {
	MsgType  MsgType `json:"msgType"`
	Data     float64 `json:"data"`
	GameTick int     `json:"gameTick,omitempty"`
}

type TurboMessage struct {
	MsgType MsgType `json:"msgType"`
	Data    string  `json:"data"`
}

type SwitchLaneMessage struct {
	MsgType MsgType `json:"msgType"`
	Data    string  `json:"data"`
}

type JoinMessage struct {
	MsgType MsgType  `json:"msgType"`
	Data    JoinData `json:"data"`
}
type JoinData struct {
	Name string `json:"name,omitempty"`
	Key  string `json:"key,omitempty"`
}
type JoinRaceMessage struct {
	MsgType MsgType      `json:"msgType"`
	Data    JoinRaceData `json:"data"`
}
type JoinRaceData struct {
	BotId     JoinData `json:"botId"`
	TrackName string   `json:"trackName"`
	CarCount  int      `json:"carCount"`
}

type YourCarMessage struct {
	MsgType MsgType `json:"msgType"`
	Data    CarId   `json:"data"`
}

type GameInitMessage struct {
	MsgType MsgType      `json:"msgType"`
	Data    GameInitData `json:"data"`
}

type GameInitData struct {
	Race `json:"race"`
}

type CarPositionsMessage struct {
	MsgType  MsgType       `json:"msgType"`
	Data     []CarPosition `json:"data"`
	GameTick int           `json:"gameTick,omitempty"`
}

type CarPosition struct {
	Id            CarId   `json:"id"`
	Angle         float64 `json:"angle"`
	PiecePosition `json:"piecePosition"`
}

type PiecePosition struct {
	PieceIndex      int     `json:"pieceIndex"`
	InPieceDistance float64 `json:"inPieceDistance"`
	LanePosition    `json:"lane"`
	Lap             int `json:"lap"`
}

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type LanePosition struct {
	StartLaneIndex int `json:"startLaneIndex"`
	EndLaneIndex   int `json:"endLaneIndex"`
}

type Race struct {
	Track       Track       `json:"track,omitempty"`
	Cars        []Car       `json:"cars,omitempty"`
	RaceSession RaceSession `json:"raceSession,omitempty"`
}

type Track struct {
	Name   string       `json:"name"`
	Lanes  []Lane       `json:"lanes"`
	Pieces []TrackPiece `json:"pieces"`
}

type Lane struct {
	DistanceFromCenter int `json:"distanceFromCenter"`
	Index              int `json:"index"`
}

type TrackPiece struct {
	Length float64 `json:"length"`
	Radius float64 `json:"radius"`
	Angle  float64 `json:"angle"`
	Switch bool    `json:"switch"`
}

type Car struct {
	Id         CarId      `json:"id"`
	Dimensions Dimensions `json:"dimensions"`
}

type Dimensions struct {
}

type RaceSession struct {
}

type SpawnMessage struct {
	GameTick int `json:"gameTick"`
}

func sendThrottleWithTick(f float64, t int) {
	buf, err := json.Marshal(ThrottleMessage{
		MsgType:  Throttle,
		Data:     f,
		GameTick: t,
	})
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(buf))
	out <- buf
}

func sendThrottle(f float64) {
	buf, err := json.Marshal(ThrottleMessage{
		MsgType: Throttle,
		Data:    f,
	})
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(buf))
	out <- buf
}

func sendTurbo() {
	buf, err := json.Marshal(TurboMessage{
		MsgType: Turbo,
		Data:    "wut wut",
	})
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(buf))
	out <- buf
}

func sendSwitchLane(l string) {
	buf, err := json.Marshal(SwitchLaneMessage{
		MsgType: SwitchLane,
		Data:    l,
	})
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(string(buf))
	out <- buf
}

func sendJoin(name string) {
	buf, err := json.Marshal(
		JoinMessage{
			MsgType: Join,
			Data:    JoinData{Name: name, Key: "rktVA/M6cEoaXg"},
		})
	/*
		buf, err := json.Marshal(
			JoinRaceMessage{
				MsgType: JoinRace,
				Data: JoinRaceData{
					BotId:     JoinData{Name: name, Key: "rktVA/M6cEoaXg"},
					TrackName: "keimola",
					CarCount:  1,
				},
			})
	*/
	if err != nil {
		log.Fatal(err)
	}
	out <- buf
}
