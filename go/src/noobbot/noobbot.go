package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math"
	"net"
	"os"
)

// struct to hold the state of the race; maybe not good design, but
// easier to have a bunch of globals to avoid passing a hundred things
// around--helps break out functionality in functions without having
// to pay that price
type RaceState struct {
	// specified by us, unique with color
	carName string
	// in case of name collision, unique with name
	// assigned at start of race
	carColor string

	// whether we're currently crashed waiting to spawn
	crashed bool

	// distance to next curve (if we're on a curve, the next one)
	distanceToNextCurve float64

	// distance in current track piece
	inPieceDistance float64
	// last tick's distance in current track piece--only used in velocity/acceleration calculations when the track piece is the same
	lastInPieceDistance float64

	// current velocity
	velocity float64
	// last tick's velocity
	lastVelocity float64
	// current acceleration
	acceleration float64
	// last tick's acceleration
	lastAcceleration float64
	// current slip angle
	angle float64
	// last tick's angle
	lastAngle float64

	// current throttle value
	throttle float64

	// track piece index we're on
	pieceIdx int
	// last track piece index we were on; helps keep track of
	// whether we have been on the same piece--some calculations
	// only need to be done once per piece
	lastPieceIdx int

	// current lap, zero-based
	lap int

	// whether turbo is available
	turboAvailable bool

	// current game tick
	gameTick int
}

// global state, should only be accessed by main thread
// without copying, else need to consider synchronization
var state RaceState

// global channels for inter-thread communication;
// basically buffers for the I/O threads
var out chan []byte
var in chan []byte

// the outbound I/O goroutine function reads from
// the outbound buffer and writes to the TCP connection
func sender(c net.Conn) {
	for {
		select {
		case msg := <-out:
			//fmt.Println("writeraw:", string(msg))
			c.Write(msg)
			c.Write([]byte("\n"))
		}
		// probably need to adjust this?
		//time.Sleep(1 * time.Millisecond)
	}
}

// reads messages from the TCP connection
// and buffers them into the channel for processing
func reader(c net.Conn) {
	for {
		buf, err := bufio.NewReader(c).ReadString('\n')
		//fmt.Println("readraw:", buf)
		if err != nil {
			if err == io.EOF {
				log.Fatal(err)
			}
			fmt.Println(err)
		}
		in <- []byte(buf)
	}
}

// the main function and loop
func main() {
	host := os.Args[1]
	port := os.Args[2]

	// connect to the host specified in the command args
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		fmt.Println(err)
	}

	// create channels with 10 slot buffers
	in = make(chan []byte, 10)
	out = make(chan []byte, 10)

	// start I/O goroutines
	go reader(conn)
	go sender(conn)

	state = RaceState{carName: "Box of Win", throttle: .5}
	sendJoin(state.carName)

	// initialize a game init message (becomes container for
	// things assigned during the game init message from the server)
	gi := GameInitMessage{}
	// the track from the game init message--convenience sub-struct
	track := Track{}

	// the main message handling loop. it tries to read a message from
	// the in channel, and then detects type, and handles the different
	// message types in the switch
	for {
		select {
		case msg := <-in:
			if !state.crashed {
				//fmt.Println(string(msg))
			}
			b := &BaseMessage{}
			err := json.Unmarshal(msg, &b)
			if err != nil {
				log.Println("couldn't parse JSON:", err)
				sendThrottle(state.throttle)
			}
			switch b.MsgType {
			case YourCar:
				yourCar := YourCarMessage{}
				err := json.Unmarshal(msg, &yourCar)
				if err != nil {
					log.Fatal(err)
				}
				state.carColor = yourCar.Data.Color
				sendThrottle(state.throttle)
			case GameInit:
				err := json.Unmarshal(msg, &gi)
				if err != nil {
					log.Fatal(err)
				}
				track = gi.Data.Race.Track
				sendThrottle(state.throttle)
				fmt.Println(string(msg))
				fmt.Println(`gameTick,inPieceDistance,velocity,acceleration,angle,totalAngle,curveRadius,distanceToCurve,throttle,pieceIdx`)
			case CarPositions:
				cp := CarPositionsMessage{}
				err := json.Unmarshal(msg, &cp)
				if err != nil {
					log.Fatal(err)
				}
				if len(track.Pieces) > 0 && state.carColor != "" {
					handleCarPositions(track, cp)
				} else {
					sendThrottle(state.throttle)
				}
			case TournamentEnd:
				return
			case Crash:
				//fmt.Println("CRASH")
				state.crashed = true
				//fmt.Printf("CRASH STATS: %3.3f, %3.4f, %3.2f, %d\n", state.velocity, state.acceleration, state.angle, state.pieceIdx)
				sendThrottle(state.throttle)
			case Spawn:
				state.crashed = false
				sm := SpawnMessage{}
				err := json.Unmarshal(msg, &sm)
				if err != nil {
					log.Fatal(err)
				}
				sendThrottle(state.throttle)
			case TurboStart:
				//fmt.Println("TURBO START")
				state.turboAvailable = false
				sendThrottle(state.throttle)
			case TurboAvailable:
				//fmt.Println("TURBO AVAILABLE")
				state.turboAvailable = true
				sendThrottle(state.throttle)
			default:
				//fmt.Println("unhandled default message...")
				//fmt.Println(string(msg))
				sendThrottle(state.throttle)
			}
		}
	}
}

func handleCarPositions(track Track, cp CarPositionsMessage) {
	// figure out which car we are in the CarPositions Data array
	myCarIdx := -1
	for i, v := range cp.Data {
		if v.Id.Name == state.carName && v.Id.Color == state.carColor {
			myCarIdx = i
			break
		}
	}
	carPos := cp.Data[myCarIdx]

	state.lap = carPos.PiecePosition.Lap
	state.gameTick = cp.GameTick
	state.pieceIdx = carPos.PiecePosition.PieceIndex
	state.inPieceDistance = carPos.PiecePosition.InPieceDistance
	state.angle = carPos.Angle

	// if we're looking at the same piece, calculate some stats
	if state.pieceIdx == state.lastPieceIdx {
		state.velocity = state.inPieceDistance - state.lastInPieceDistance
		state.acceleration = state.velocity - state.lastVelocity
	}

	// if we're in a new piece, see if we need to switch lanes soon
	// need to rework this logic when we take into account other
	// car positions
	if state.pieceIdx != state.lastPieceIdx {
		switchLaneLogic(track, carPos, cp.Data)
	}

	// figure out what to set the throttle to
	state.throttle = getThrottle(track, carPos, cp.Data)

	// need to make sure we send a valid gameTick value;
	// 0 isn't valid and caused the server to throw it back
	if cp.GameTick == 0 {
		sendThrottle(state.throttle)
	} else {
		sendThrottle(state.throttle)
	}

	// set last values to be the current values after sending the throttle message--we'll process again in another tick
	state.lastPieceIdx = state.pieceIdx
	state.lastInPieceDistance = state.inPieceDistance
	state.lastVelocity = state.velocity
	state.lastAngle = state.angle
	state.lastAcceleration = state.acceleration
}

// figure out whether we want to switch lanes, and send a switch
// lane message if so
// TODO set a status in state if we decide to switch
// so that the throttle function can use it for calculations
func switchLaneLogic(track Track, carPos CarPosition, cp []CarPosition) {
	// we need to look one piece ahead, because you can't start switching lanes in the piece that has the switch
	pIdx := state.pieceIdx + 1
	if pIdx > len(track.Pieces)-1 {
		pIdx = 0
	}

	lanePos := carPos.PiecePosition.LanePosition
	// distance until next switch in current lane
	currentDistance := 0.0
	// distance until next switch in next left lane
	leftDistance := 0.0
	// distance until next switch in next right lane
	rightDistance := 0.0

	leftLaneIdx := -1
	rightLaneIdx := -1
	canShiftLeft := false
	canShiftRight := false
	if lanePos.EndLaneIndex > 0 {
		leftLaneIdx = lanePos.EndLaneIndex - 1
		canShiftLeft = true
	}
	if lanePos.EndLaneIndex < len(track.Lanes)-1 {
		rightLaneIdx = lanePos.EndLaneIndex + 1
		canShiftRight = true
	}

	// only bother checking the switch logic if there is a switch
	// and we're not already shifting
	if track.Pieces[pIdx].Switch && lanePos.StartLaneIndex == lanePos.EndLaneIndex {
		// start iterating through the track pieces to
		// calculate total length, i is our index iterator
		i := pIdx
		for {
			// if track piece isn't a curve, do simple +=
			// TODO handle switch distances
			if track.Pieces[i].Length > 0.1 {
				currentDistance += track.Pieces[i].Length
				if canShiftLeft {
					leftDistance += track.Pieces[i].Length
				}
				if canShiftRight {
					rightDistance += track.Pieces[i].Length
				}
			} else {
				// if track pieces are a curve...
				// it's a bit more complicated
				currentDistance += getCurvePieceLength(track, lanePos.EndLaneIndex, i)
				if canShiftLeft {
					left := getCurvePieceLength(track, leftLaneIdx, i)
					if i == pIdx {
						// get average between current and other length...
						// not sure if it's right but seems to work
						cur := getCurvePieceLength(track, lanePos.EndLaneIndex, i)
						leftDistance += (cur + left) / 2
					} else {
						leftDistance += left
					}
				}
				if canShiftRight {
					right := getCurvePieceLength(track, rightLaneIdx, i)
					if i == pIdx {
						// get average between current and other length...
						// not sure if it's right but seems to work
						cur := getCurvePieceLength(track, lanePos.EndLaneIndex, i)
						rightDistance += (cur + right) / 2
					} else {
						rightDistance += right
					}
				}
			}
			i++
			if i > len(track.Pieces)-1 {
				i = 0
			}
			if track.Pieces[i].Switch {
				break
			}
		}
		//fmt.Println(lanePos, currentDistance, leftDistance, rightDistance)
		if canShiftLeft && currentDistance > leftDistance {
			sendSwitchLane("Left")
			//fmt.Println("switch lane left!")
		}
		if canShiftRight && currentDistance > rightDistance {
			sendSwitchLane("Right")
			//fmt.Println("switch lane right!")
		}
	}
}

func getCurvePieceLength(track Track, laneIdx int, pieceIdx int) float64 {
	if track.Pieces[pieceIdx].Angle > 0.0 {
		return (track.Pieces[pieceIdx].Radius - float64(track.Lanes[laneIdx].DistanceFromCenter)) * 3.14159 / 180 * math.Abs(track.Pieces[pieceIdx].Angle)
	} else {
		return (track.Pieces[pieceIdx].Radius + float64(track.Lanes[laneIdx].DistanceFromCenter)) * 3.14159 / 180 * math.Abs(track.Pieces[pieceIdx].Angle)
	}
}

func getThrottle(track Track, carPos CarPosition, cp []CarPosition) float64 {
	distanceToCurve := 0.0
	pIdx := carPos.PiecePosition.PieceIndex
	inPieceDistance := carPos.PiecePosition.InPieceDistance
	pLen := track.Pieces[pIdx].Length
	if pLen > 0.1 {
		distanceToCurve += pLen - inPieceDistance
	}
	i := pIdx + 1
	if i > len(track.Pieces)-1 {
		i = 0
	}
	// we're on a curve
	if track.Pieces[pIdx].Radius > 0.1 {
		distanceToCurve = 0
		i = pIdx
	}
	curveRadius := 0.0
	for {
		if track.Pieces[i].Radius > 0.1 {
			curveRadius = track.Pieces[i].Radius
			break
		}
		distanceToCurve += track.Pieces[i].Length
		i++
		if i > len(track.Pieces)-1 {
			i = 0
		}
	}
	totalAngle := track.Pieces[i].Angle
	curveStart := i
	for {
		i++
		if i > len(track.Pieces)-1 {
			i = 0
		}
		if track.Pieces[curveStart].Radius != track.Pieces[i].Radius || math.Signbit(track.Pieces[i].Angle) != math.Signbit(totalAngle) {
			break
		}
		totalAngle += track.Pieces[i].Angle
	}

	if state.velocity > 0.00001 {
		fmt.Printf("%04d,%03.5f,%03.5f,%03.5f,%03.5f,%03.2f,%03.2f,%03.5f,%03.2f,%d\n",
			state.gameTick, state.inPieceDistance, state.velocity,
			state.acceleration, state.angle, totalAngle, curveRadius,
			distanceToCurve, state.throttle, pIdx)
	}
	if distanceToCurve > 300 && math.Abs(state.lastAngle)-math.Abs(state.angle) > 0 && state.angle < 50 && state.lap > 0 && state.turboAvailable {
		//fmt.Println("SEND TURBO MESSAGE")
		state.turboAvailable = false
		sendTurbo()
	}
	angleDiff := math.Abs(state.lastAngle) - math.Abs(state.angle)
	if angleDiff > 0.1 && state.angle < 50 && state.velocity < 10 {
		if track.Pieces[state.pieceIdx].Length < 0.1 && math.Signbit(track.Pieces[state.pieceIdx].Angle) == math.Signbit(totalAngle) || state.angle >= 40 {
			//fmt.Println("coming out of turn...")
			return 1.0
		}
	}
	_ = curveRadius
	//calculate safe speed for curve
	// TODO make this general
	maxVel := 6.1
	if curveRadius <= 50.0 && pIdx == 9 && state.velocity > maxVel-1.7 {
		return 0.01
	}
	if curveRadius <= 50.0 && pIdx == 11 && state.velocity > maxVel-1.6 {
		return 0.01
	}
	if curveRadius <= 50.0 && state.velocity > maxVel-1.5 && distanceToCurve < 150 && math.Abs(totalAngle) <= 50 {
		return 0.01
	}
	if curveRadius <= 50.0 && state.velocity > maxVel-1.6 && distanceToCurve < 150 && math.Abs(totalAngle) >= 50 {
		return 0.01
	}
	if state.velocity > maxVel+2.9 && distanceToCurve < 135 && math.Abs(totalAngle) >= 90 {
		return 0.01
	}
	if state.velocity > maxVel && distanceToCurve < 50 && math.Abs(totalAngle) >= 90 {
		return 0.01
	}
	if state.velocity > maxVel+.5 && distanceToCurve < 50 && math.Abs(totalAngle) <= 90 {
		return 0.01
	}
	return 1.0
}
